﻿
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;


namespace Examen_2do_Parcial
{

    //Jhustin Ismael Arias Perez 4-A T/M

    class Program
    {
        private static Semaphore pooldechalanes;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;

        //Procedimiento albañiles

        private static void ChalanAlbanil(object chalan)
        {
            Console.WriteLine("Chalan de Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
            Thread.CurrentThread.ManagedThreadId,
            Thread.CurrentThread.ThreadState,
            Thread.CurrentThread.Priority);
            Console.WriteLine("Chalan {0} esperando turno...", chalan);
            pooldechalanes.WaitOne();
            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 100);
            Console.WriteLine("Chalan {0} obtiene turno...", chalan);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Chalan {0} termina turno...", chalan);
            Console.WriteLine("Chalan {0} libera su lugar {1}",
            chalan, pooldechalanes.Release());
        }

        //Para que se ejecute el main de forma asincronica
        public static async Task Main(string[] args)
        {
            {
                Console.WriteLine("¿Cuántos chalanes llegaron a chambear?");
                n = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("¿Cuántos pueden trabajar?");
                m = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("¿Cuántos segundos hay para construir?");
                o = Convert.ToInt32(Console.ReadLine()) * 1000;

                pooldechalanes = new Semaphore(0, m);

                Console.WriteLine("Inicia la construcción");

                for (int i = 1; i <= n; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(ChalanAlbanil));
                    if (i % 2 == 0)
                    {
                        t.Priority = ThreadPriority.AboveNormal;
                    }
                    else
                    {
                        t.Priority = ThreadPriority.BelowNormal;
                    }
                    t.Start(i);
                }

                Thread.Sleep(500);
                Console.WriteLine("Se libera un turno");
                pooldechalanes.Release(m);
                Thread.Sleep(o);

                Construccion_Casa();
                task_ConstruccionAsync();
            }

            //Procedimiento donde le pido al usuario el numero de casas, habitaciones y baños que desea construir
            //Se imprime el numero pedido

            void Construccion_Casa()
            {

                        Console.WriteLine("\n¿Cuantas casas desea construir?: ");
                        int casa = Convert.ToInt32(Console.ReadLine());
                        int[] Num_Casa = new int[casa];
     
                        for (int u = 0; u < Num_Casa.Length; u++)
                        {
                            Console.WriteLine("\n¿Cuantas habitaciones desea construir en la casa {0}?: ", u+1);
                            int habita = Convert.ToInt32(Console.ReadLine());
                            int[] Num_Habita = new int[habita];

                            Console.WriteLine("\n¿Cuantos baños desea construir en la casa {0}?", u+1);
                             int baño = Convert.ToInt32(Console.ReadLine());
                             int[] Num_Baño = new int[baño];

                            Console.WriteLine("\nUsted pidio que se construyeran: ");
                            Console.WriteLine("{0} Habitaciones",habita);
                            Console.WriteLine("{0} Baños",baño);

                        }

            }

            //Task del procedimiento Construccion_Casa()

            async Task task_ConstruccionAsync()
            {

                Task Casa_Terminada = Task.Run(() => { Construccion_Casa(); });
                Task Baño_Terminado = Task.Run(() => { Construccion_Casa(); });
                Task Cuarto_Terminado = Task.Run(() => { Construccion_Casa(); });

                var Task_CSH = new List<Task> { Casa_Terminada, Baño_Terminado, Cuarto_Terminado };

                //Este task me permitira mostrar como poco a poco como se construyen casas, baños y habitaciones

                while (Task_CSH.Count > 0)
                {
                    Task finishedTask = await Task.WhenAny(Task_CSH);

                    if (finishedTask == Casa_Terminada)
                    {
                        Console.WriteLine("Espere Por Favor....");
                        Thread.Sleep(900);
                        Console.WriteLine("Casas Listas");
                    }
                    else if (finishedTask == Baño_Terminado)
                    {
                        Console.WriteLine("Espere Por Favor....");
                        Thread.Sleep(900);
                        Console.WriteLine("Baños Listos");

                    }
                    else if (finishedTask == Cuarto_Terminado)
                    {
                        Console.WriteLine("Espere Por Favor....");
                        Thread.Sleep(900);
                        Console.WriteLine("Habitaciones Listas");
                    }
                    Task_CSH.Remove(finishedTask);
                }
                Console.WriteLine("Construccion Terminada");
                Console.ReadKey();

            }

        }

    }

        }
    

